import kotlinx.coroutines.experimental.*
import utils.RETROFIT

fun main(args: Array<String>) = runBlocking {
    val host = "https://vassuv.ru/api/test/style"
    val params = hashMapOf("id" to "5")

    val result = RETROFIT.get(host, params)
    println(result)
}