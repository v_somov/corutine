import kotlinx.coroutines.experimental.*
import retrofit2.Call
import retrofit2.Callback
import utils.RETROFIT
import utils.trace
import java.rmi.UnknownHostException
import kotlin.coroutines.experimental.suspendCoroutine

fun main(args: Array<String>) = runBlocking {
    val host = "https://vassuv.ru/api/test/style/"

    trace("Start")
    runBlocking {
        val results = List(11) {
            async {
                host.responseWithMap(hashMapOf("id" to it.toString())){
                    it.length.toString()
                }
            }
        }

        results.forEach { println(it.await()) }
    }
}

private suspend fun String.response(params: HashMap<String, String>) = responseWithMap(params) {it}

private suspend fun <T>String.responseWithMap(params: HashMap<String, String>, map: (String)-> T): Response<T> {
    return try {
        withTimeoutOrNull(20000) {
            withContext(CommonPool) {
                Response(RETROFIT.get(this@responseWithMap, params).await(map))
            }
        } ?: Response<T>(null, TIMEOUT_ERROR)
    } catch (throwable: Throwable) {
        Response(null, when (throwable) {
            is UnknownHostException -> INTERNET_ERROR
            is Error -> throwable
            else -> ERROR
        })
    }
}

data class Response<out T>(val result: T?,
                           val error: Error = EMPTY_ERROR)

data class Error(val status: Int = 0,
            override val message: String?) : Throwable()

val EMPTY_ERROR = Error(0, null)
val TIMEOUT_ERROR = Error(900, "Timeout Error")
val INTERNET_ERROR = Error(800, "Internet Error")
val ERROR = Error(1000, "Error")

private suspend fun <T> Call<String>.await(map:  ((String)->T)): T = suspendCoroutine { continuation ->
    enqueue(object : Callback<String> {
        override fun onResponse(call: Call<String>?, response: retrofit2.Response<String>?) {
            val body: String? = response?.body()
            if (body == null) {
                continuation.resumeWithException(ERROR)
            } else {
                continuation.resume(map(body))
            }
        }

        override fun onFailure(call: Call<String>?, t: Throwable?) {
            if (t == null) {
                continuation.resumeWithException(ERROR)
            } else {
                continuation.resumeWithException(t)
            }
        }
    })
}