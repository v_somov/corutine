package examples

import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - SimpleExample.kt")
    loadPage()
}

private fun loadPage() {
    val page = requestPage()
    val style = requestStyle()

    viewPage(page, style)
}

private fun requestPage(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private fun requestStyle() : String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}