package examples

import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - CallbackHellExample.kt")
    loadPage()
}

private fun loadPage() {
    requestPage { page ->
        requestStyle { style ->
            viewPage(page, style)
        }
    }
}

private fun requestPage(block: (String?) -> Unit) {
    block(RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body())
}

private fun requestStyle(block: (String?) -> Unit) {
    block(RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body())
}
