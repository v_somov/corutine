package examples

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.withTimeoutOrNull
import utils.RETROFIT
import utils.trace

/* Timeout suspending */

fun main(args: Array<String>) = runBlocking {
//    withTimeout(5000L) {
    withTimeoutOrNull(5000L) {
        repeat(100) {
            if (!isActive) return@repeat

            trace(" ---> id=$it")
            requestStyle(it)
            delay(1)
        }
    }

    println("Stop")
}

private suspend fun requestStyle(id: Int): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/", hashMapOf("id" to id.toString())).execute().body()
}
