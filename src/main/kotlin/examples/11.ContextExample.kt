package examples

import kotlinx.coroutines.experimental.*
import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    runBlocking {
        trace("Start ContextExample.kt\n")
        val jobs = arrayListOf<Job>()
        jobs += launch(Unconfined) {
            requestPage("Unconfined")
            trace("'Unconfined' working in thread ${Thread.currentThread().name}")
        }
        jobs += launch(DefaultDispatcher) {
            requestPage("Default")
            trace("'Default' working in thread ${Thread.currentThread().name}")
        }
        jobs += launch(coroutineContext) {
            requestPage("coroutineContext")
            trace("'coroutineContext' working in thread ${Thread.currentThread().name}")
        }
        jobs += launch(CommonPool) {
            requestPage("CommonPool")
            trace("'CommonPool' working in thread ${Thread.currentThread().name}")
        }
        jobs += launch(newSingleThreadContext("MyOwnThread")) {
            requestPage("newSTC")
            trace("'newSTC' working in thread ${Thread.currentThread().name}")
        }
        jobs.forEach { it.join() }
    }
}

private suspend fun requestPage(context: String): String? {
    trace(" ---> '$context'")
    return RETROFIT.get("https://vassuv.ru/api/test/page/$context").execute().body()
}
