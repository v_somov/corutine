package examples

import utils.RETROFIT
import utils.trace
import kotlin.concurrent.thread

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - ThreadExample.kt")
    loadPage()
}

private fun loadPage() {
    var page: String? = null
    var style: String? = null

    val pageThread = thread {
        page = requestPage()
    }

    val styleThread = thread {
        style = requestStyle()
    }

    pageThread.join()
    styleThread.join()

    viewPage(page, style)
}

private fun requestPage(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private fun requestStyle() : String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}
