package examples

import io.reactivex.Single
import utils.RETROFIT
import utils.trace
import utils.traceException

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - RxExample.kt")
    loadPage()
}

private fun loadPage() {
    var page: String? = null
    var style: String? = null

    requestPage()
            .doOnError { traceException("requestPage", it) }
            .doOnSuccess { page = it }
            .flatMap { requestStyle() }
            .doOnError { traceException("requestStyle", it) }
            .doOnSuccess { style = it }
            .blockingGet()

    viewPage(page, style)
}

private fun requestPage(): Single<String?> {
    return Single.create { emitter ->
        emitter.onSuccess(RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body())
    }
}

private fun requestStyle(): Single<String?> {
    return Single.create { emitter ->
        emitter.onSuccess(RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body())
    }
}