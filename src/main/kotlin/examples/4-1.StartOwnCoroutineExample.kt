package examples

import utils.RETROFIT
import utils.trace
import kotlin.coroutines.experimental.*

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - CoroutineExample.kt")
    runBlocking {
        loadPage()
    }
}

private suspend fun loadPage() {
    val page = requestPage()
    val style = requestStyle()
    viewPage(page, style)
}

private suspend fun requestPage(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private suspend fun requestStyle(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}

typealias CC = CoroutineContext
typealias CE = CoroutineContext.Element
typealias CK<T> = CoroutineContext.Key<T>

private fun runBlocking(block: suspend () -> Unit) {
    block.startCoroutine(object : Continuation<Unit> {
        override val context = object : CC {
            val list = hashMapOf<Any, CE>()

            override fun <R> fold(initial: R, operation: (R, CE) -> R) = operation(initial, object : CE {
                override val key = object : CK<CE> {}
            })

            override fun <E : CE> get(key: CK<E>): E? {
                return list[key] as E?
            }

            override fun minusKey(key: CK<*>): CC {
                list.remove(key)
                return this
            }
        }

        override fun resume(value: Unit) {}

        override fun resumeWithException(exception: Throwable) {}
    })
}

