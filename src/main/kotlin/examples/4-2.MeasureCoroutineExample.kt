package examples

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT
import utils.trace
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    RETROFIT
    runBlocking {
        loadPage()
    }
}

private suspend fun loadPage() {
    val time = measureTimeMillis {
        trace("Start - MeasureCoroutineExample.kt")
        val page = requestPage()
        val style = requestStyle()

        trace("View Page")
        viewPage(page, style)
    }
    println("Completed in $time ms")
}

private suspend fun requestPage(): String? {
    delay(1500)
    return ""//RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private suspend fun requestStyle(): String? {
    delay(1000)
    return ""//RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}
