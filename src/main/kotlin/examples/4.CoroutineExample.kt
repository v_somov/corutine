package examples

import utils.RETROFIT
import utils.trace
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) {
    RETROFIT
    trace("Start - CoroutineExample.kt")
    runBlocking {
        loadPage()
    }
}

private suspend fun loadPage() {
    val page = requestPage()
    val style = requestStyle()
    viewPage(page, style)
}

private suspend fun requestPage(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private suspend fun requestStyle(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}