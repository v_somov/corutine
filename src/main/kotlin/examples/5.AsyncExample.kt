package examples

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    RETROFIT
    runBlocking {
        loadPage()
    }
}

private suspend fun loadPage() {
    trace("Start - AsyncExample.kt")
    val pageAsync = async { requestPage() }
    val styleAsync = async { requestStyle() }

    trace("View Page")
    viewPage(pageAsync.await(), styleAsync.await())
    trace("Viewed Page")
}

private suspend fun requestPage(): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private suspend fun requestStyle() : String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}