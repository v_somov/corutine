package examples

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    loadPage()
}

private fun loadPage() {
    trace("Start")

    val pageAsync = requestPage()
    val styleAsync = requestStyle()

    runBlocking {
        viewPage(pageAsync.await(), styleAsync.await())
    }
}

private fun requestPage() = async {
    return@async RETROFIT.get("https://vassuv.ru/api/test/page/").execute().body()
}

private fun requestStyle() = async {
    return@async RETROFIT.get("https://vassuv.ru/api/test/style/").execute().body()
}