package examples

import kotlinx.coroutines.experimental.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.RETROFIT
import utils.trace
import kotlin.coroutines.experimental.suspendCoroutine

fun main(args: Array<String>) {
    RETROFIT
    trace("Start MultiAsyncExample.kt")

    runBlocking {
        List(50) { id ->
            trace(" ---> id=$id")
            requestStyle(id.toString())
        }

        delay(10000)
    }
    trace("End - MultiAsyncExample.kt")
}

private fun requestStyle(id: String) = async(CommonPool) {
    suspendCoroutine<String?> { continuation ->
        RETROFIT.get("https://vassuv.ru/api/test/style/", hashMapOf("id" to id)).enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<String>?, response: Response<String>?) {
                continuation.resume(response?.body())
            }
        })
    }
}
