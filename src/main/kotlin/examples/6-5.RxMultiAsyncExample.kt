package examples

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.RETROFIT
import utils.trace

fun main(args: Array<String>) {
    RETROFIT
    loadPage()
}

private fun loadPage() {

    trace("Start - RxMultiAsyncExample.kt")

    Observable.range(1,50)
            .flatMapSingle { value -> requestStyle(value.toString())
                    .subscribeOn(Schedulers.io())}
            .blockingForEach {  }

    trace("End - RxMultiAsyncExample.kt")
}

private fun requestStyle(id: String) = Single.create<Unit> { emitter ->
    trace(" ---> id=$id")
    emitter.onSuccess(RETROFIT.get("https://vassuv.ru/api/test/style/", hashMapOf("id" to id)).enqueue(callback))
}

private val callback = object : Callback<String> {
    override fun onFailure(call: Call<String>?, t: Throwable?) {
    }

    override fun onResponse(call: Call<String>?, response: Response<String>?) {
    }
}
