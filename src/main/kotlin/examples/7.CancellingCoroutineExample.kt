package examples

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT
import utils.trace

/* Making computation code cancellable */

fun main(args: Array<String>) = runBlocking {
    val job = launch {
        repeat(100) {
            trace(" ---> id=$it")
            requestStyle(it)
        }
    }

    delay(2000)

    job.cancel()
    return@runBlocking
}

private suspend fun requestStyle(id: Int): String? {
    return RETROFIT.get("https://vassuv.ru/api/test/style/", hashMapOf("id" to id.toString())).execute().body()
}
