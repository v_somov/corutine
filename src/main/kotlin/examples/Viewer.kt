package examples

import utils.trace

fun viewPage(page: String?, style: String?) {
    trace("View Page")
    println("============================================")
    if (page != null)
        println("‖                  P A G E                 ‖")
    if (style != null)
        println("‖                 S T Y L E                ‖")
    println("============================================")
//    println(page)
//    println(style)
}