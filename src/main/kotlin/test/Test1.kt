package test

import utils.RETROFIT

fun main(args: Array<String>) {
    val page = requestPage()
    viewPage(page)
}

private fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()

private fun viewPage(page: String?) {
}