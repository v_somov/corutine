package test

import utils.RETROFIT

fun main(args: Array<String>) {

    val page = requestPage()
    val style = requestStyle()

    viewPage(page, style)
}

private fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()

private fun requestStyle() = RETROFIT.get("https://ok.ru/style.css").execute().body()

private fun viewPage(page: String?, style: String?) {
}