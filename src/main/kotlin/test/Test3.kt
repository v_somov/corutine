package test

import utils.RETROFIT
import kotlin.concurrent.thread


fun main(args: Array<String>) {
    var page: String? = null
    var style: String? = null

    val pageLoadThread = thread {
        page = requestPage()
    }

    val styleLoadThread = thread {
        style = requestStyle()
    }

    pageLoadThread.join()
    styleLoadThread.join()

    // parse page, parse style
    // other operations
}

private fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()

private fun requestStyle() = RETROFIT.get("https://ok.ru/style.css").execute().body()