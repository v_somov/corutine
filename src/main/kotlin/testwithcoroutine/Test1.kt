package testwithcoroutine

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT

fun main(args: Array<String>) {
    var page: String?

    val job: Job = launch {
        page = requestPage()
    }

//    runBlocking {
//        job.join()
//    }

    // parse page
    // other operations
}

private suspend fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()