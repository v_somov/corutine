package testwithcoroutine

import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT

fun main(args: Array<String>) {
    var page: String?
    var style: String?

    val job = launch {
        page = requestPage()
        style = requestStyle()
    }

    runBlocking {
        job.join()
    }

    // parse page, parse style
    // other operations
}

private fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()

private fun requestStyle() = RETROFIT.get("https://ok.ru/style.css").execute().body()