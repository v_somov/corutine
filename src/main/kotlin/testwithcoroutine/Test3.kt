package testwithcoroutine

import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import utils.RETROFIT

fun main(args: Array<String>) {
    var page: String? = null
    var style: String? = null

    val pageJob = launch {
        page = requestPage()
    }

    val styleJob = launch {
        style = requestStyle()
    }

    runBlocking {
        pageJob.join()
        styleJob.join()
    }

    // parse page, parse style
    // other operations
}

private suspend fun requestPage() = RETROFIT.get("https://ok.ru").execute().body()

private suspend fun requestStyle() = RETROFIT.get("https://ok.ru/style.css").execute().body()