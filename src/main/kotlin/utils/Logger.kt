package utils

@JvmField
var debugMode: Boolean = true

private var tag: String = "MyApp"

var time = 0L

fun getTime(time: Long) = "%6d".format(time)//SimpleDateFormat("mm:ss.mmm").format(Date(time))

fun trace(vararg args: String?) {
    val currentTime = System.currentTimeMillis()

    if (time == 0L) time = currentTime

    if (debugMode) println("${getTime(currentTime - time)}: ${varargToString(args)}")
}

fun traceException(text: String, exception: Throwable) {
    if (debugMode) {
        println("$tag $text: $exception")
        exception.stackTrace.forEach { println("$tag |   $it") }
    }
}

private fun varargToString(args: Array<*>): String = args.joinToString(separator = " ") {
    if (it is Array<*>) varargToString(it) else it.toString()
}