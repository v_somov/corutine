package utils

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.withContext
import kotlinx.coroutines.experimental.withTimeoutOrNull
import retrofit2.Call
import retrofit2.Callback
import java.net.UnknownHostException
import kotlin.coroutines.experimental.suspendCoroutine


private class Response<T>(@JvmField var result: T? = null,
                  @JvmField var error: Error = ERROR_EMPTY)

private class Error ( @JvmField var status: Int,
             @JvmField var errorText: String? = null) : Exception()

private val INTERNET_NOT_FOUND = Error(status = 800, errorText = "Отсутсвует интернет соединение")
private val TIMEOUT_ERROR = Error(status = 801, errorText = "Превышено время ожидания")
private val ERROR = Error(status = 1000, errorText = "Произошла ошибка")
private val ERROR_EMPTY = Error(status = 0, errorText = null)

private suspend fun getResponse(s: String, hashMapOf: HashMap<String, String>): Response<String> = try {
    withTimeoutOrNull(10000) {
        withContext(CommonPool) {
            Response(RETROFIT.get(s, hashMapOf).await())
        }
    } ?: Response( error= TIMEOUT_ERROR)
} catch (throwable: Throwable) {
    traceException("await Failure", throwable)
    throwable.toErrorResponse()
}

private inline suspend fun String.response(hashMapOf: HashMap<String, String>,  block: Response<String>.() -> Unit)
        = getResponse("https://vassuv.ru/api/v1/" + this, hashMapOf).block()

private fun <T> Throwable.toErrorResponse() = Response<T>( error= when(this) {
    is UnknownHostException -> INTERNET_NOT_FOUND
    is Error -> this
    else -> ERROR
})

suspend private fun Call<String>.await() = suspendCoroutine<String> { cont ->
    enqueue(object : Callback<String> {
        override fun onFailure(call: Call<String>?, t: Throwable?) {
            t?.let { throwable ->
                cont.resumeWithException(throwable)
            }
        }

        override fun onResponse(call: Call<String>?, response: retrofit2.Response<String>?) {
            response?.body()?.let { body ->
                trace(body)
                if (body.isNotEmpty())
                    cont.resume(body)
                else
                    cont.resumeWithException(ERROR)
            }
        }
    })
}